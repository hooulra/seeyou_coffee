package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.enums.SellProductType;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.sellproduct.*;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.SellProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "판매 상품 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sell-product")
public class SellProductController {
    private final SellProductService sellProductService;

    @PostMapping("/new")
    @ApiOperation(value = "판매 상품 등록")
    public CommonResult setSellProduct(@RequestBody @Valid SellProductRequest sellProductRequest) {
        sellProductService.setSellProduct(sellProductRequest);
        return ResponseService.getSuccessResult();
    }
    @PutMapping("/update/{id}")
    @ApiOperation(value = "판매 상품 수정")
    public CommonResult putSellProduct(@PathVariable long id, @RequestBody @Valid SellProductUpdateRequest request) {
        sellProductService.putSellProduct(id, request);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{page}")
    @ApiOperation(value = "판매 상품 리스트")
    public ListResult<SellProductItem> getSellProduct(
            @PathVariable int page,
            @RequestParam(value = "searchType", required = false) SellProductType searchType,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        SellProductSearchRequest request = new SellProductSearchRequest();
        request.setSellProductType(searchType);
        request.setProductName(searchName);
        return ResponseService.getListResult(sellProductService.getList(page, request), true);
    }

    @GetMapping("/product")
    @ApiOperation(value = "판매 상품 리스트 상세")
    public SingleResult<SellProductResponse> getProduct(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(sellProductService.getSellProduct(id));
    }
}
