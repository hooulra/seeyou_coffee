package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.SellProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellProductRepository extends JpaRepository<SellProduct, Long> {
    Page<SellProduct> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
