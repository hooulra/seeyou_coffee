package com.seeyou.seeyouapi.model.sellproduct;

import com.seeyou.seeyouapi.entity.SellProduct;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellProductItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품타입")
    private String sellProductType;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    private SellProductItem(Builder builder) {
        this.id = builder.id;
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
    }

    public static class Builder implements CommonModelBuilder<SellProductItem> {
        private final Long id;
        private final String sellProductType;
        private final String productName;
        private final BigDecimal unitPrice;

        public Builder(SellProduct sellProduct) {
            this.id = sellProduct.getId();
            this.sellProductType = sellProduct.getSellProductType().getName();
            this.productName = sellProduct.getProductName();
            this.unitPrice = sellProduct.getUnitPrice();
        }

        @Override
        public SellProductItem build() {
            return new SellProductItem(this);
        }
    }
}
