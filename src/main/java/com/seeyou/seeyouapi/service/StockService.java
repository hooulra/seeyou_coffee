package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.entity.Stock;
import com.seeyou.seeyouapi.exception.CAccessDeniedException;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.stock.StockItem;
import com.seeyou.seeyouapi.model.stock.StockResponse;
import com.seeyou.seeyouapi.model.stock.StockRequest;
import com.seeyou.seeyouapi.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StockService {
    private final StockRepository stockRepository;

    // 재고 등록
    public void setStock(Product product, StockRequest request) {
        stockRepository.save(new Stock.Builder(product, request).build());
    }

    // 재고 리스트
    public ListResult<StockItem> getStockList(int page) {
        Page<Stock> originData = stockRepository.findAllByIdGreaterThanEqualOrderByStockQuantity(1, ListConvertService.getPageable(page));


        List<StockItem> result = new LinkedList<>();
        originData.forEach(e -> result.add(new StockItem.Builder(e).build()));


        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    public StockResponse getStock(long id) {
        Stock stock = stockRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new StockResponse.Builder(stock).build();
    }

    // 완료된 발주 상품 자동 수량 증가
    public void putAutoUpdateStock(ProductOrder productOrder) {
        Stock stock = stockRepository.findByProduct(productOrder.getProduct());
        stock.putAutoUpdateStock(productOrder.getQuantity());
        stockRepository.save(stock);
    }

    // 재고 수량, 최소 재고 수량 수정
    public void putStock(long id, StockRequest request) {
        Stock stock = stockRepository.findById(id).orElseThrow(CAccessDeniedException::new);
        stock.putStock(request);

        stockRepository.save(stock);
    }

    // 부족한 재고 수량 리스트
    public ListResult<StockResponse> getStockLackList() {
        List<Stock> originData = stockRepository.findAll();
        List<StockResponse> result = new LinkedList<>();

        for (Stock e : originData) {
            if (e.getStockQuantity() <= e.getMinQuantity()) {
                result.add(new StockResponse.Builder(e).build());
            }
        }
        return ListConvertService.settingResult(result);
    }
}
