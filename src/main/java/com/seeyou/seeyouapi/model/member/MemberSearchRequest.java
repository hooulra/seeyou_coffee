package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class MemberSearchRequest {
    private String memberName;

    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    private Boolean isEnabled;
}
