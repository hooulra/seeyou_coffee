package com.seeyou.seeyouapi.model.money;

import com.seeyou.seeyouapi.entity.MoneyHistory;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyHistoryResponse {
    @ApiModelProperty(notes = "직원명")
    private String member;

    @ApiModelProperty(notes = "연도")
    private Integer moneyYear;

    @ApiModelProperty(notes = "월")
    private String moneyMonth;

    @ApiModelProperty(notes = "세전금액")
    private BigDecimal beforeMoney;

    @ApiModelProperty(notes = "국민연금")
    private BigDecimal nationalPension;

    @ApiModelProperty(notes = "건강보험")
    private BigDecimal healthInsurance;

    @ApiModelProperty(notes = "장기요양보험")
    private BigDecimal longTermCareInsurance;

    @ApiModelProperty(notes = "고용보험")
    private BigDecimal employmentInsurance;

    @ApiModelProperty(notes = "산재보험")
    private BigDecimal industrialAccidentInsurance;

    @ApiModelProperty(notes = "소득세")
    private BigDecimal incomeTax;

    @ApiModelProperty(notes = "지방소득세")
    private BigDecimal localIncomeTax;

    @ApiModelProperty(notes = "총공제액")
    private BigDecimal totalDeductionAmount;

    @ApiModelProperty(notes = "급여")
    private BigDecimal money;

    private MoneyHistoryResponse(Builder builder) {
        this.member = builder.member;
        this.moneyYear = builder.moneyYear;
        this.moneyMonth = builder.moneyMonth;
        this.beforeMoney = builder.beforeMoney;
        this.nationalPension = builder.nationalPension;
        this.healthInsurance = builder.healthInsurance;
        this.longTermCareInsurance = builder.longTermCareInsurance;
        this.employmentInsurance = builder.employmentInsurance;
        this.industrialAccidentInsurance = builder.industrialAccidentInsurance;
        this.incomeTax = builder.incomeTax;
        this.localIncomeTax = builder.localIncomeTax;
        this.totalDeductionAmount = builder.totalDeductionAmount;
        this.money = builder.money;
    }

    public static class Builder implements CommonModelBuilder<MoneyHistoryResponse> {
        private final String member;
        private final Integer moneyYear;
        private final String moneyMonth;
        private final BigDecimal beforeMoney;
        private final BigDecimal nationalPension;
        private final BigDecimal healthInsurance;
        private final BigDecimal longTermCareInsurance;
        private final BigDecimal employmentInsurance;
        private final BigDecimal industrialAccidentInsurance;
        private final BigDecimal incomeTax;
        private final BigDecimal localIncomeTax;
        private final BigDecimal totalDeductionAmount;
        private final BigDecimal money;

        public Builder(MoneyHistory moneyHistory) {
            this.member = moneyHistory.getMember().getMemberName();
            this.moneyYear = moneyHistory.getMoneyYear();
            this.moneyMonth = moneyHistory.getMoneyMonth();
            this.beforeMoney = moneyHistory.getBeforeMoney();
            this.nationalPension = moneyHistory.getNationalPension();
            this.healthInsurance = moneyHistory.getHealthInsurance();
            this.longTermCareInsurance = moneyHistory.getLongTermCareInsurance();
            this.employmentInsurance = moneyHistory.getEmploymentInsurance();
            this.industrialAccidentInsurance = moneyHistory.getIndustrialAccidentInsurance();
            this.incomeTax = moneyHistory.getIncomeTax();
            this.localIncomeTax = moneyHistory.getLocalIncomeTax();
            this.totalDeductionAmount = nationalPension.add(healthInsurance).add(longTermCareInsurance).add(employmentInsurance).add(industrialAccidentInsurance).add(incomeTax).add(localIncomeTax);
            this.money = moneyHistory.getMoney().subtract(totalDeductionAmount);
        }
        @Override
        public MoneyHistoryResponse build() {
            return new MoneyHistoryResponse(this);
        }
    }
}