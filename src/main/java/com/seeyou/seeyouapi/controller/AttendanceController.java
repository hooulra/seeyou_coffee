package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.model.attendance.AttendanceItem;
import com.seeyou.seeyouapi.model.attendance.AttendanceSearchRequest;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.member.MemberSearchRequest;
import com.seeyou.seeyouapi.service.AttendanceService;
import com.seeyou.seeyouapi.service.ProfileService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.Struct;

@RequiredArgsConstructor
@RequestMapping("/v1/attendance")
@RestController
@Api(tags = "근태 관리")
public class AttendanceController {
    private final AttendanceService attendanceService;
    private final ProfileService profileService;

    @PostMapping("/new")
    @ApiOperation("근태 등록")
    public CommonResult setAttendance() {
        Member member = profileService.getMemberData();
        attendanceService.setAttendance(member);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/leave-work")
    @ApiOperation(value = "퇴근하기")
    public CommonResult putLeaveWork() {
        Member member = profileService.getMemberData();
        attendanceService.putLeaveWork(member);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{page}")
    @ApiOperation(value = "근태 리스트")
    public ListResult<AttendanceItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchDay", required = false) String searchDay
            ) {
        AttendanceSearchRequest request = new AttendanceSearchRequest();
        request.setAttendanceYear(searchYear);
        request.setAttendanceMonth(searchMonth);
        request.setAttendanceDay(searchDay);
        return ResponseService.getListResult(attendanceService.getList(page, request), true);
    }

    @GetMapping("/my")
    @ApiOperation(value = "나의 근태 리스트")
    public ListResult<AttendanceItem> getMyList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        Member member = profileService.getMemberData();
        return ResponseService.getListResult(attendanceService.getMyList(page, member), true);
    }
}
