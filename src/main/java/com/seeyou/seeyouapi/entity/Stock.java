package com.seeyou.seeyouapi.entity;

import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.model.stock.StockRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer stockQuantity;

    @Column(nullable = false)
    private Integer minQuantity;

    public void putAutoUpdateStock(int quantity) {
        this.stockQuantity += quantity;
    }

    public void putStock(StockRequest request) {
        this.stockQuantity = request.getStockQuantity();
        this.minQuantity = request.getMinQuantity();
    }

    private Stock(Builder builder) {
        this.product = builder.product;
        this.stockQuantity = builder.stockQuantity;
        this.minQuantity = builder.minQuantity;
    }

    public static class Builder implements CommonModelBuilder<Stock> {
        private final Product product;
        private final Integer stockQuantity;
        private final Integer minQuantity;

        public Builder(Product product, StockRequest request) {
            this.product = product;
            this.stockQuantity = request.getStockQuantity();
            this.minQuantity = request.getMinQuantity();
        }

        @Override
        public Stock build() {
            return new Stock(this);
        }
    }
}
