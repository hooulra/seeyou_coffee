package com.seeyou.seeyouapi.model.sellproduct;

import com.seeyou.seeyouapi.enums.SellProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellProductSearchRequest {
    private SellProductType sellProductType;
    private String productName;
}
