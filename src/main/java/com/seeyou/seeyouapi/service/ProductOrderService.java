package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderCompleteItem;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderItem;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderResponse;
import com.seeyou.seeyouapi.model.productOrder.ProductOrderRequest;
import com.seeyou.seeyouapi.repository.ProductOrderRepository;
import com.seeyou.seeyouapi.repository.SettlementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductOrderService {
    private final ProductOrderRepository productOrderRepository;

    public ProductOrder getProductData(long id) {
        return productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    // 발주 등록
    public void setProductOrder(Product product, ProductOrderRequest request) {
        productOrderRepository.save(new ProductOrder.Builder(product, request).build());
    }

    // 발주 리스트 불러오기
    public ListResult<ProductOrderItem> getProductOrderList(int page) {
        Page<ProductOrder> originData = productOrderRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<ProductOrderItem> result = new LinkedList<>();

        originData.forEach(e -> result.add(new ProductOrderItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }

    public ProductOrderResponse getProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ProductOrderResponse.Builder(productOrder).build();
    }

    // 발주 상태 수정
    public void putProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        productOrder.putProductOrder();
        productOrderRepository.save(productOrder);
    }

    public ListResult<ProductOrderCompleteItem> getCompleteProductPageList(int page) {
        Page<ProductOrder> originData = productOrderRepository.findAllByIdGreaterThanEqualAndIsCompleteTrueOrderByCompleteDateOrderDesc(1, ListConvertService.getPageable(page));
        List<ProductOrderCompleteItem> result = new LinkedList<>();

        originData.forEach(e -> result.add(new ProductOrderCompleteItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originData.getTotalElements(), originData.getTotalPages(), originData.getPageable().getPageNumber());
    }
}
