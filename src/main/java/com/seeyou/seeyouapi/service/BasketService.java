package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Basket;
import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.model.basket.BasketRequest;
import com.seeyou.seeyouapi.repository.BasketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BasketService {
    private final BasketRepository basketRepository;

    public void setBasket(Product product, BasketRequest request) {
        Basket basket = new Basket.Builder(product, request).build();
        basketRepository.save(basket);
    }
}
