package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "직원명")
    private String memberName;

    @ApiModelProperty(notes = "직급")
    private String memberGroup;

    @ApiModelProperty(notes = "연락처")
    private String contact;

    @ApiModelProperty(notes = "재직여부")
    private String isEnabled;

    private MemberItem(Builder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberGroup = builder.memberGroup;
        this.contact = builder.contact;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberName;
        private final String memberGroup;
        private final String contact;
        private final String isEnabled;

        public Builder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.memberGroup = member.getMemberGroup().getRankName();
            this.contact = member.getContact();
            this.isEnabled = member.getIsEnabled() ? "O" : "X";
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
