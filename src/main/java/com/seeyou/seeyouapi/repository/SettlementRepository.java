package com.seeyou.seeyouapi.repository;

import com.seeyou.seeyouapi.entity.Settlement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface SettlementRepository extends JpaRepository<Settlement, Long> {
    List<Settlement> findAllBySettlementYearAndSettlementMonth(int year, String month);
    Page<Settlement> findAllByIdGreaterThanEqualOrderByIdDesc(long id, Pageable pageable);
}
