package com.seeyou.seeyouapi.model.dataTest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataTestDetailResponse {
    private Long id;
    private String name;
    private String phone;
    private String contents;
}
