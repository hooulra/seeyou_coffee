package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.entity.Money;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.money.MoneyItem;
import com.seeyou.seeyouapi.model.money.MoneyRequest;
import com.seeyou.seeyouapi.model.money.MoneyResponse;
import com.seeyou.seeyouapi.repository.MoneyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;

    public void setMoney(Member member, MoneyRequest moneyRequest) {
        Money money = new Money.Builder(member, moneyRequest).build();
        moneyRepository.save(money);
    }

    public ListResult<MoneyItem> getMoneyList(int page) {
        Page<Money> originList = moneyRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));

        List<MoneyItem> result = new LinkedList<>();

        for (Money money : originList) {
            result.add(new MoneyItem.Builder(money).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }
    public MoneyResponse getMoneyLists(long id) {
        Money money = moneyRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyResponse.Builder(money).build();
    }

    public void putMoney(long id, MoneyRequest moneyRequest) {
        Money money = moneyRepository.findById(id).orElseThrow(CMissingDataException::new);
        money.putMoney(moneyRequest);
        moneyRepository.save(money);
    }
}
