package com.seeyou.seeyouapi.controller;

import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.settlement.SettlementItem;
import com.seeyou.seeyouapi.model.settlement.SettlementResponse;
import com.seeyou.seeyouapi.service.SettlementService;
import com.seeyou.seeyouapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "정산 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/settlement")
public class SettlementController {
    private final SettlementService settlementService;

    @ApiOperation(value = "정산 리스트")
    @GetMapping("/all")
    public ListResult<SettlementItem> getList(@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        return ResponseService.getListResult(settlementService.getList(page), true);
    }

    @ApiOperation(value = "정산액 보기")
    @GetMapping("/settlement")
    public SingleResult<SettlementResponse> getSettlement(@RequestParam("year") int year, @RequestParam("month") String month) {
        return ResponseService.getSingleResult(settlementService.getSettlement(year, month));
    }

    @ApiOperation(value = "정산 수정")
    @PutMapping("/update")
    public CommonResult putSettlement(@RequestParam("year") int year, @RequestParam("month") String month) {
        settlementService.putSettlement(year, month);
        return ResponseService.getSuccessResult();
    }
}
