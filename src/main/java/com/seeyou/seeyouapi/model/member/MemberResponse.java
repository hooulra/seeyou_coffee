package com.seeyou.seeyouapi.model.member;

import com.seeyou.seeyouapi.entity.Member;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import com.seeyou.seeyouapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberResponse {
    @ApiModelProperty(notes = "직원명")
    private String memberName;

    @ApiModelProperty(notes = "직급")
    private String memberGroup;

    @ApiModelProperty(notes = "연락처")
    private String contact;

    @ApiModelProperty(notes = "입사일")
    private String dateCreate;

    @ApiModelProperty(notes = "퇴사일")
    private String dateEnd;

    @ApiModelProperty(notes = "재직여부")
    private String isEnabled;

    private MemberResponse(Builder builder) {

        this.memberName = builder.memberName;
        this.memberGroup = builder.memberGroup;
        this.contact = builder.contact;
        this.dateCreate = builder.dateCreate;
        this.dateEnd = builder.dateEnd;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<MemberResponse> {

        private final String memberName;

        private final String memberGroup;

        private final String contact;

        private final String dateCreate;

        private final String dateEnd;

        private final String isEnabled;


        public Builder(Member member) {
            this.memberName = member.getMemberName();
            this.memberGroup = member.getMemberGroup().getRankName();
            this.contact = member.getContact();
            this.dateCreate = CommonFormat.convertLocalDateToString(member.getDateCreate());
            this.dateEnd = CommonFormat.convertLocalDateToString(member.getDateEnd());
            this.isEnabled = member.getIsEnabled() ? "O" : "X" ; // ? = if , 앞의 값은 true , 뒤에 값은 false
        }
        @Override
        public MemberResponse build() {
            return new MemberResponse(this);
        }
    }
}
