package com.seeyou.seeyouapi.model.stock;

import com.seeyou.seeyouapi.entity.Stock;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StockItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "재고수량")
    private Integer stockQuantity;

    private StockItem(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.stockQuantity = builder.stockQuantity;
    }

    public static class Builder implements CommonModelBuilder<StockItem> {
        private final Long id;
        private final String productName;
        private final Integer stockQuantity;

        public Builder(Stock stock) {
            this.id = stock.getId();
            this.productName = stock.getProduct().getProductName();
            this.stockQuantity = stock.getStockQuantity();
        }

        @Override
        public StockItem build() {
            return new StockItem(this);
        }
    }
}
