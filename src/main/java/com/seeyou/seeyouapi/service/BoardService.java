package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Board;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.board.BoardItem;
import com.seeyou.seeyouapi.model.board.BoardResponse;
import com.seeyou.seeyouapi.model.board.BoardRequest;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(BoardRequest request) {
        Board board = new Board.Builder(request).build();
        boardRepository.save(board);
    }

    public ListResult<BoardItem> getList(int page) {
        Page<Board> originList = boardRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1, ListConvertService.getPageable(page));
        List<BoardItem> result = new LinkedList<>();
        for (Board board : originList) {
            result.add(new BoardItem.Builder(board).build());
        }
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    public BoardResponse getBoard(long id) {
        Board board = boardRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new BoardResponse.Builder(board).build();
    }

    public void putBoard(long id, BoardRequest request) {
        Board board = boardRepository.findById(id).orElseThrow();
        board.putBoard(request);
        boardRepository.save(board);
    }

    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
