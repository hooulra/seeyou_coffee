package com.seeyou.seeyouapi.model.productOrder;

import com.seeyou.seeyouapi.entity.ProductOrder;
import com.seeyou.seeyouapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderResponse {
    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private Integer quantity;

    @ApiModelProperty(notes = "수량")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "발주일")
    private String dateOrder;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private ProductOrderResponse(Builder builder) {
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderResponse> {
        private final String productName;
        private final Integer quantity;
        private final BigDecimal unitPrice;
        private final BigDecimal price;
        private final String dateOrder;
        private final String isComplete;

        public Builder(ProductOrder productOrder) {
            this.productName = productOrder.getProduct().getProductName();
            this.quantity = productOrder.getQuantity();
            this.unitPrice = productOrder.getProduct().getUnitPrice();
            this.price = productOrder.getPrice();
            this.dateOrder = productOrder.getOrderYear() + "-" + productOrder.getOrderMonth() + "-" + productOrder.getOrderDay() + " " +  productOrder.getTimeOrder().getHour() + ":" + productOrder.getTimeOrder().getMinute();
            this.isComplete = productOrder.getIsComplete() ? "O" : "X";
        }
        @Override
        public ProductOrderResponse build() {
            return new ProductOrderResponse(this);
        }
    }
}
