package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Income;
import com.seeyou.seeyouapi.entity.Sell;
import com.seeyou.seeyouapi.model.profit.ProfitResponse;
import com.seeyou.seeyouapi.repository.IncomeRepository;
import com.seeyou.seeyouapi.repository.SellRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfitService {
    private final SellRepository sellRepository;
    private final IncomeRepository incomeRepository;

    public ProfitResponse getProfit(String year, String month) {
        List<Sell> sellList = sellRepository.findAllBySellYearAndSellMonth(year, month);
        List<Income> incomeList = incomeRepository.findAllByIncomeYearAndIncomeMonth(year, month);
        BigDecimal sells = BigDecimal.valueOf(0);
        BigDecimal incomes = BigDecimal.valueOf(0);
        for (Sell sell : sellList) {
            sells = sells.add(sell.getPrice());
        }
        for (Income income : incomeList) {
            incomes = incomes.add(income.getPrice());
        }
        return new ProfitResponse.Builder(sells.subtract(incomes)).build();
    }
}
