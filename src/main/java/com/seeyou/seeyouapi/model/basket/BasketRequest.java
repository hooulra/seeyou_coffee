package com.seeyou.seeyouapi.model.basket;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketRequest {
    private Integer quantity;
}
