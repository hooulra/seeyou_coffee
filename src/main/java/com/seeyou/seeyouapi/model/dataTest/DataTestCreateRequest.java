package com.seeyou.seeyouapi.model.dataTest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DataTestCreateRequest {
    @ApiModelProperty(notes = "이름", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phone;

    @ApiModelProperty(notes = "금액", required = true)
    @NotNull
    private Double price;

    @ApiModelProperty(notes = "나이", required = true)
    @NotNull
    private Integer age;
}
