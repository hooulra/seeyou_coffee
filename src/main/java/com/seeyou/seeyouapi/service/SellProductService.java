package com.seeyou.seeyouapi.service;

import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.entity.SellProduct;
import com.seeyou.seeyouapi.exception.CMissingDataException;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.product.ProductItem;
import com.seeyou.seeyouapi.model.product.ProductSearchRequest;
import com.seeyou.seeyouapi.model.sellproduct.*;
import com.seeyou.seeyouapi.repository.SellProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SellProductService {
    private final SellProductRepository sellProductRepository;
    private final EntityManager entityManager;

    public void setSellProduct(SellProductRequest sellProductRequest) {
        SellProduct sellProduct = new SellProduct.Builder(sellProductRequest).build();
        sellProductRepository.save(sellProduct);
    }
    public void putSellProduct(long id, SellProductUpdateRequest request) {
        SellProduct sellProduct = sellProductRepository.findById(id).orElseThrow(CMissingDataException::new);
        sellProduct.putSellProduct(request);
        sellProductRepository.save(sellProduct);
    }

    public ListResult<SellProductItem> getList(int page, SellProductSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<SellProduct> originList = getList(pageRequest, request);

        List<SellProductItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new SellProductItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<SellProduct> getList(Pageable pageable, SellProductSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SellProduct> criteriaQuery = criteriaBuilder.createQuery(SellProduct.class);

        Root<SellProduct> root = criteriaQuery.from(SellProduct.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getSellProductType() != null) predicates.add(criteriaBuilder.equal(root.get("sellProductType"), request.getSellProductType()));
        if (request.getProductName() != null) predicates.add(criteriaBuilder.like(root.get("productName"), "%" + request.getProductName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<SellProduct> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public SellProductResponse getSellProduct(long id) {
        SellProduct sellProduct = sellProductRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new SellProductResponse.Builder(sellProduct).build();
    }
}
