package com.seeyou.seeyouapi.controller;


import com.seeyou.seeyouapi.entity.Product;
import com.seeyou.seeyouapi.model.common.CommonResult;
import com.seeyou.seeyouapi.model.common.ListResult;
import com.seeyou.seeyouapi.model.common.SingleResult;
import com.seeyou.seeyouapi.model.stock.StockItem;
import com.seeyou.seeyouapi.model.stock.StockResponse;
import com.seeyou.seeyouapi.model.stock.StockRequest;
import com.seeyou.seeyouapi.service.ProductService;
import com.seeyou.seeyouapi.service.ResponseService;
import com.seeyou.seeyouapi.service.StockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "재고 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/stock")
public class StockController {
    private final StockService stockService;
    private final ProductService productService;

    @ApiOperation(value = "재고 등록")
    @PostMapping("/product-id/{productId}")
    public CommonResult setStock(@PathVariable long productId, @RequestBody @Valid StockRequest request) {
        Product product = productService.getProductData(productId);
        stockService.setStock(product, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "재고 리스트")
    @GetMapping("/list")
    public ListResult<StockItem> getStockList(@RequestParam(value = "page", defaultValue = "1") int page) {
        return ResponseService.getListResult(stockService.getStockList(page), true);
    }

    @ApiOperation(value = "재고 리스트 상세")
    @GetMapping("/stock")
    public SingleResult<StockResponse> getStock(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(stockService.getStock(id));
    }

    @ApiOperation(value = "재고 수정")
    @PutMapping("/update/{id}")
    public CommonResult putStock(@PathVariable long id, @RequestBody @Valid StockRequest request) {
        stockService.putStock(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "재고 부족 리스트")
    @GetMapping("/lack-list")
    public ListResult<StockResponse> getStockLackList() {
       return ResponseService.getListResult(stockService.getStockLackList(), true);
    }
}
